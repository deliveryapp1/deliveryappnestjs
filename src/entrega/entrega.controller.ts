import { EntregaService } from './shared/entrega.service';
import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from '@nestjs/common';
import { Entrega } from './shared/entrega';

@Controller('entrega')
export class EntregaController {
  constructor(private entregaService: EntregaService) {}

  @Get()
  async getAll() {
    return await this.entregaService.getAll();
  }

  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.entregaService.getById(id);
  }

  @Post()
  async create(@Body() entrega: Entrega) {
    return await this.entregaService.create(entrega);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() entrega: Entrega) {
    return this.entregaService.update(id, entrega);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.entregaService.delete(id);
  }
}
