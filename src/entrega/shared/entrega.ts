import { Document } from 'mongoose';
export class Entrega extends Document {
  bairro: string;
  tempoEntrega: number;
  entregaGratis: boolean;
  valorEntrega: number;
}
