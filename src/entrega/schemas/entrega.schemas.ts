import * as mongoose from 'mongoose';
export const EntregaSchema = new mongoose.Schema({
  bairro: String,
  tempoEntrega: Number,
  entregaGratis: Boolean,
  valorEntrega: Number,
});
