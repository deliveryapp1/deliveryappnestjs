import { EntregaController } from './entrega.controller';
import { EntregaSchema } from './schemas/entrega.schemas';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { EntregaService } from './shared/entrega.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Entrega', schema: EntregaSchema }]),
  ],
  controllers: [EntregaController],
  providers: [EntregaService],
})
export class EntregaModule {}
