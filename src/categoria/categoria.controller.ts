import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
} from '@nestjs/common';
import { CategoriaService } from './shared/categoria.service';
import { Categoria } from './shared/categoria';

@Controller('categoria')
export class CategoriaController {
  constructor(private categoriaService: CategoriaService) {}

  @Get()
  async getAll() {
    return await this.categoriaService.getAll();
  }

  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.categoriaService.getById(id);
  }

  @Post()
  async create(@Body() categoria: Categoria) {
    return await this.categoriaService.create(categoria);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() categoria: Categoria) {
    return this.categoriaService.update(id, categoria);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.categoriaService.delete(id);
  }
}
