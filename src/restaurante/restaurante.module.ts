import { RestauranteService } from './shared/restaurante.service';
import { RestauranteController } from './restaurante.controller';
import { RestauranteSchema } from './schemas/restaurante.schemas';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Restaurante', schema: RestauranteSchema },
    ]),
  ],
  controllers: [RestauranteController],
  providers: [RestauranteService],
})
export class RestauranteModule {}
