import * as mongoose from 'mongoose';
export const RestauranteSchema = new mongoose.Schema({
  semana: Number,
  horaInicio: Date,
  horaFim: Date,
});
