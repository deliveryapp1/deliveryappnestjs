import { Document } from 'mongoose';
export class Restaurante extends Document {
  semana: number;
  horaInicio: Date;
  horaFim: Date
}