import { Restaurante } from './restaurante';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class RestauranteService {
  constructor(
    @InjectModel('Restaurante')
    private readonly restauranteModel: Model<Restaurante>,
  ) {}

  async getAll() {
    return await this.restauranteModel.find().exec();
  }

  async getById(id: string) {
    return await this.restauranteModel.findById(id).exec();
  }

  async create(restaurante: Restaurante) {
    const createdEntrega = new this.restauranteModel(restaurante);
    return await createdEntrega.save();
  }

  async update(id: string, restaurante: Restaurante) {
    return await this.restauranteModel.findByIdAndUpdate(id, restaurante, {
      new: true,
    });
  }

  async delete(id: string) {
    return await this.restauranteModel.deleteOne({ _id: id }).exec();
  }
}
