import { RestauranteService } from './shared/restaurante.service';
import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import { Restaurante } from './shared/restaurante';

@Controller('restaurante')
export class RestauranteController {
  constructor(private restauranteService: RestauranteService) {}

  @Get()
  async getAll() {
    return await this.restauranteService.getAll();
  }

  @Get(':id')
  async getById(@Param('id') id: string) {
    return await this.restauranteService.getById(id);
  }

  @Post()
  async create(@Body() restaurante: Restaurante) {
    return await this.restauranteService.create(restaurante);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() restaurante: Restaurante) {
    return this.restauranteService.update(id, restaurante);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.restauranteService.delete(id);
  }
}
