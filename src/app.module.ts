import { RestauranteModule } from './restaurante/restaurante.module';
import { EntregaModule } from './entrega/entrega.module';
import { CategoriaModule } from './categoria/categoria.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
        RestauranteModule,
    EntregaModule,
    CategoriaModule,
    MongooseModule.forRoot(
      'mongodb+srv://db_user:zhKyeT4KS41Q1NAY@deliveryapp-xp615.gcp.mongodb.net/deliveryapp?retryWrites=true&w=majority',
      { useFindAndModify: false },
    ),
  ],
  controllers: [
        AppController],
  providers: [AppService],
})
export class AppModule {}
